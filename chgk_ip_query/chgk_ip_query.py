#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import codecs
import argparse
import csv
import os
import sys
import time
import datetime
import json
import toml
import itertools
from collections import defaultdict
import requests
try:
    import gspread
except ImportError:
    sys.stderr.write(
        "gspread is not present, google sheets processing will not be available\n"
    )

try:
    import pyexcel
except ImportError:
    sys.stderr.write(
        "pyexcel is not present, excel export will not be available\n"
    )
import phonenumbers
from phonenumbers.phonenumberutil import region_code_for_number


class IpQuery:
    def __init__(self, ip_cache_path):
        if not ip_cache_path:
            ip_cache_path = os.path.join(
                os.path.dirname(os.path.realpath(__file__)), "ip_cache.json"
            )
        self.ip_cache_path = ip_cache_path
        if os.path.exists(ip_cache_path):
            with open(ip_cache_path) as f:
                self.ip_cache = json.load(f)
        else:
            self.ip_cache = {}

    @staticmethod
    def get_resource_dir():
        if getattr(sys, "frozen", False):
            sourcedir = os.path.dirname(sys.executable)
            resourcedir = os.path.join(sourcedir, "resources")
        else:
            sourcedir = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))
            resourcedir = os.path.join(sourcedir, "resources")
        return sourcedir, resourcedir

    def query_ips(self, ips):
        ips_ = [x for x in ips if x not in self.ip_cache]
        parts = []
        while len(ips_) > 100:
            parts.append(ips_[:100])
            ips_ = ips_[100:]
        parts.append(ips_)
        for part in parts:
            payload = [
                {"query": ip, "fields": "query,countryCode,regionName,city"}
                for ip in part
            ]
            req = requests.post("http://ip-api.com/batch", json=payload)
            try:
                j = req.json()
            except json.decoder.JSONDecodeError:
                raise Exception(
                    "status {}, request content {}".format(
                        req.status_code, req.content
                    )
                )
            for response in j:
                try:
                    self.ip_cache[response["query"]] = "{} {}, {}".format(
                        response["countryCode"],
                        response["regionName"],
                        response["city"],
                    )
                except KeyError:
                    raise Exception("ips: {}".format(ips_))
            if len(parts) > 1:
                time.sleep(5)
        with open(self.ip_cache_path, "w") as f:
            json.dump(self.ip_cache, f, indent=2, sort_keys=True)

    def query_ip(self, ip):
        return self.ip_cache.get(ip)


def get_rus_phone_numbers(filename):
    result = defaultdict(dict)
    with codecs.open(filename, "r", "utf8") as csvfile:
        reader = csv.reader(csvfile, delimiter=";", quotechar='"')
        for row in itertools.islice(reader, 1, None):
            result[int(row[0])][(int(row[1]), int(row[2]))] = "{} {}".format(
                row[4], row[5]
            )
    return result


def get_rus_pn_info(pn, rus_phone_numbers):
    initial_code = int(pn[2:5])
    rpn = rus_phone_numbers.get(initial_code)
    if not rpn:
        return "UNKNOWN"
    rest = int(pn[5:])
    for range_ in rpn:
        if range_[0] <= rest <= range_[1]:
            return rpn[range_]
    return "UNKNOWN"


def get_info_by_phone_number(pn, rus_phone_numbers):
    pn = pn.split(": ")[-1]
    pn = pn.replace(" ", "")
    pn = pn.replace("-", "")
    pn = pn.replace("(", "")
    pn = pn.replace(")", "")
    if pn.startswith("+79"):
        return get_rus_pn_info(pn, rus_phone_numbers)
    elif pn.startswith("89"):
        return get_rus_pn_info("+79{}".format(pn[2:]), rus_phone_numbers)
    try:
        parsed = phonenumbers.parse(pn)
    except phonenumbers.phonenumberutil.NumberParseException:
        return "UNKNOWN_PHONE_NUMBER_ERROR"
    return region_code_for_number(parsed)


class RequestsProcessor:
    URL = "https://rating.chgk.info/synch.php?download_data=download_requests&format=csv&tournament_id="

    def __init__(self, args):
        self.args = args
        self.printer = None
        self.now = datetime.datetime.now().strftime("%Y-%m-%dT%H-%M-%S")
        if self.args.config_file:
            self.config_dir = os.path.dirname(os.path.abspath(self.args.config_file))
        else:
            self.config_dir = None

    def make_output_filename(self, filename, fileformat, suffix="output"):
        basename, _ = os.path.splitext(filename)
        result = f"{basename}_{suffix}"
        if self.now not in result:
            result += f"_{self.now}"
        result += f'.{"xlsx" if fileformat == "excel" else "csv"}'
        return result

    def read_blacklist_range(self, table_key):
        table = self.gc.open_by_key(table_key)
        sh = table.worksheet("Бан")
        recs = sh.get_all_records()
        transformed = {
            str(x["ID"]).strip(): x for x in recs if x["ID"]
        }
        return transformed

    def print(self, s):
        print(s)
        if self.args.print_to_file is not None and self.printer is None:
            self.printer = open(self.args.print_to_file, "w", encoding="utf8")
        if self.printer:
            self.printer.write((s or "") + "\n")

    def download_rating_data(self):
        ids = self.args.rating_ids.split(",")
        s = requests.Session()
        for k, v in self.args.cookies.items():
            s.cookies.set(k, v, domain="rating.chgk.info")

        if not os.path.isdir("ip_query_output"):
            os.mkdir("ip_query_output")

        for id_ in ids:
            req = s.get(self.URL + id_)
            assert req.status_code == 200
            if req.text.startswith("<!DOCTYPE"):
                sys.stderr.write("Authentication error, please refresh cookies file\n")
                sys.exit(1)
            filename = os.path.join("ip_query_output", f"tournament-synch-requests-{id_}-{self.now}.csv")
            with open(filename, "w", encoding="utf8") as f:
                f.write(req.text)
            self.args.input_files.append(filename)

    def init_gc(self):
        self.gc = gspread.service_account(self.args.json_keyfile_name)

    def process_file(self, input_file):
        if self.config_dir and not os.path.isabs(input_file):
            input_file = os.path.abspath(os.path.join(self.config_dir, input_file))

        if not self.args.input_encoding:
            self.args.input_encoding = "cp1251" if self.args.style == "old" else "utf8"
        if not self.args.output_encoding:
            self.args.output_encoding = "utf8"

        if self.args.config_file and self.args.def_path:
            cwd = os.getcwd()
            os.chdir(os.path.dirname(os.path.abspath(self.args.config_file)))
            def_path = os.path.abspath(self.args.def_path)
            os.chdir(cwd)
        elif self.args.def_path:
            def_path = self.args.def_path
        else:
            def_path = None

        if def_path and os.path.isfile(def_path):
            path = def_path
        elif os.path.isfile(path := os.path.join(self.get_resource_dir(), "DEF-9xx.csv")):
            pass
        elif os.path.isfile(path := os.path.join(os.getcwd(), "DEF-9xx.csv")):
            pass
        elif os.path.isfile(path := os.path.join(os.path.dirname(input_file), "DEF-9xx.csv")):
            pass
        else:
            raise Exception("DEF-9xx.csv not found")
        
        assert os.path.isfile(path)
        rus_phone_numbers = get_rus_phone_numbers(path)

        self.print(
            "opening {} with encoding {}".format(
                input_file, self.args.input_encoding
            )
        )
        with codecs.open(input_file, "r", self.args.input_encoding) as csvfile:
            delimiter = ";" if self.args.style == "old" else ","
            reader = csv.reader(csvfile, delimiter=delimiter, quotechar='"')
            result = [row for row in reader]

        result[0].append("Регион IP")
        result[0].append("Регион номера телефона представителя")
        result[0].append("Регион номера телефона ведущего")
        if self.args.blacklist:
            result[0].append("В чёрном списке")
        max_lendiff = 4

        if self.args.style == "old":
            ip_field = 18
        else:
            ip_field = 19

        ips = [rec[ip_field] for rec in result[1:]]
        ipq = IpQuery(self.args.ip_cache_path)
        ipq.query_ips(ips)

        black_preds = []
        black_veds = []
        clean_preds = []
        clean_veds = []
        for rec in result[1:]:
            self.print("processing {}...".format(rec[4]))
            lendiff = len(result[0]) - len(rec)
            assert 0 < lendiff <= max_lendiff
            for _ in range(max_lendiff - lendiff):
                rec.pop()
            pred_id_field = 3
            phone_pred_field = 5
            phone_ved_field = 11
            status_field = 16
            pred_email = rec[4]
            pred_name = f"{rec[6]} {rec[7]} {rec[8]}"
            ved_email = rec[10]
            ved_name = f"{rec[12]} {rec[13]} {rec[14]}"
            rec.append(ipq.query_ip(rec[ip_field]))
            rec.append(
                get_info_by_phone_number(rec[phone_pred_field], rus_phone_numbers)
            )
            rec.append(
                get_info_by_phone_number(rec[phone_ved_field], rus_phone_numbers)
            )
            if not self.args.blacklist:
                continue
            if rec[pred_id_field] in self.blacklist:
                zachto = self.blacklist[rec[pred_id_field]]["За что"]
                if rec[status_field] == "Новая":
                    black_preds.append(f"{pred_email} {pred_name} {zachto}")
                    black_veds.append(f"{ved_email} {ved_name} (у {pred_name})")
                rec.append(zachto)
            elif rec[status_field] == "Новая":
                clean_preds.append(f"{pred_email}")
                clean_veds.append(f"{ved_email}")

        if not self.args.output_file:
            self.args.output_file = self.make_output_filename(
                input_file, self.args.output_format
            )
        new_output_file = self.make_output_filename(
            input_file, self.args.output_format, suffix="new_only"
        )

        if self.args.output_format == "csv":
            self.print(
                "writing results into {} with encoding {}".format(
                    self.args.output_file, self.args.output_encoding
                )
            )
            with codecs.open(
                self.args.output_file, "w", self.args.output_encoding, errors="replace"
            ) as csvfile:
                writer = csv.writer(csvfile, delimiter=";", quotechar='"')
                for row in result:
                    writer.writerow(row)
        elif self.args.output_format == "excel":
            pyexcel.save_as(array=result, dest_file_name=self.args.output_file)
            self.print(f"output file: {self.args.output_file}")
            new_only = [result[0]] + [x for x in result if x[status_field] == "Новая"]
            pyexcel.save_as(array=new_only, dest_file_name=new_output_file)
            self.print(f"new only output file: {new_output_file}")
            self.print("\nnew clean preds:\n" + "\n".join(clean_preds))
            self.print("\nnew clean veds:\n" + "\n".join(clean_veds))
            if self.args.blacklist:
                self.print("\nblack preds:\n" + "\n".join(black_preds))
                self.print("\nblack veds:\n" + "\n".join(black_veds))

    def process(self):
        assert self.args.rating_ids or self.args.input_files
        if self.args.blacklist:
            assert os.path.isfile(self.args.json_keyfile)
            self.init_gc()
            self.blacklist = self.read_blacklist_range(self.args.blacklist)
            self.print("blacklist:" + ",".join(sorted(self.blacklist.keys())))
        else:
            self.blacklist = {}
        if self.args.rating_ids:
            if not hasattr(self.args, "cookies"):
                assert os.path.isfile(self.args.cookie_file)
                with open(self.args.cookie_file) as f:
                    self.args.cookies = json.loads(f.read())
            self.download_rating_data()
        for file_name in self.args.input_files:
            self.process_file(file_name)
        if self.printer:
            self.printer.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config_file", "-c")
    parser.add_argument("--rating_ids", "-r")
    parser.add_argument("--input_files", "-i")
    parser.add_argument("--print_to_file", "-ptf")
    parser.add_argument("--json_keyfile", "-key")
    parser.add_argument("--cookie_file", "-cookie")
    parser.add_argument("--blacklist")
    parser.add_argument("--output_file", default=None)
    parser.add_argument("--input_encoding", "-ie")
    parser.add_argument("--output_encoding", "-oe")
    parser.add_argument("--output_format", "-of", default="excel")
    parser.add_argument("--style", choices=["old", "new"], default="new")
    parser.add_argument("--def_path")
    parser.add_argument("--ip_cache_path")
    args = parser.parse_args()

    if args.config_file:
        with open(args.config_file, "r") as f:
            config = toml.loads(f.read())
        for k, v in config.items():
            setattr(args, k, v)

    if args.input_files:
        args.input_files = args.input_files.split(",")
    else:
        args.input_files = []

    processor = RequestsProcessor(args)
    processor.process()


if __name__ == "__main__":
    main()
