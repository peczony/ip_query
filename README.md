# ip_query

Для использования нужно поставить Python. Если у вас Windows, проще всего воспользоваться [установщиком последней версии третьего питона от Anaconda](https://repo.continuum.io/miniconda/Miniconda3-latest-Windows-x86_64.exe). После этого нужно запускать консоль через меню _Пуск → Anaconda Prompt_.

После установки Питона запустите Anaconda Prompt и введите команду:

`pip install requests phonenumbers`

Если хотите экспортировать в Excel, выполните также:

`pip install pyexcel pyexcel-xlsx`

Скачайте с [сайта Россвязи](http://www.rossvyaz.ru/activity/num_resurs/registerNum/) и положите в ту же папку, что и скрипт, файл [DEF-9хx.csv](https://rossvyaz.ru/data/DEF-9xx.csv)

Входной файл для работы скрипта — скачанный **с вкладки «Управление»** (а не «Заявки») сайта рейтинга csv с заявками, выглядящий как `tournament-synch-requests-IDТУРНИРА.csv`, в нашем случае для примера будет `tournament-synch-requests-4982.csv`. Поместите его в ту же папку, что и ip_query.

Перейдите в Anaconda Prompt в папку с ip_query (`cd c:\Path\To\ip_query`) и запустите программу так:

`python ip_query.py tournament-synch-requests-4982.csv`

Разумеется, вместо 4982 подставьте нужный ID турнира.

После того, как программа отработает, в папке появится файл `tournament-synch-requests-IDТУРНИРА_output.csv`, где добавятся новые колонки:

- «Регион IP», которая будет содержать регион IP по данным сайта [http://ip-api.com](ip-api.com).
- «Регион номера телефона представителя»
- «Регион номера телефона ведущего»

Две последние колонки содержат информацию о регионах номеров телефонов представителя и ведущего по данным Россвязи. Для всех стран, кроме России, туда пишется только двухбуквенный код страны (например, KZ для Казахстана).

Иногда определители IP и номера могут ошибаться, это нормально. Их можно перепроверять вручную в других сервисах (например, [iplocation.net](https://www.iplocation.net/) для IP и [gsm-inform.ru](https://gsm-inform.ru/info/) для номеров телефонов).

Если у вас есть вопросы, связаться с автором программы можно [в Телеграме](https://t.me/pecheny), [вконтакте](https://vk.me/pecheny) или по почте peczony@gmail.com. 
