from setuptools import setup


def get_version():
    version = {}
    with open("chgk_ip_query/version.py") as f:
        exec(f.read(), version)
    return version["__version__"]


long_description = """**chgk_ip_query** is an utility that helps to check requests files from rating.chgk.info."""


setup(
    name="chgk_ip_query",
    version=get_version(),
    author="Alexander Pecheny",
    author_email="peczony@gmail.com",
    description="rating.chgk.info requests checker",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/peczony/ip_query",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=["chgk_ip_query"],
    package_data={
        "chgk_ip_query": [
            "resources/*.toml",
        ]
    },
    entry_points={"console_scripts": ["chgk_ip_query = chgk_ip_query.chgk_ip_query:main"]},
    install_requires=[
        "toml",
        "gspread",
        "requests",
        "pyexcel",
        "pyexcel-xlsx",
        "phonenumbers"
    ]
)
